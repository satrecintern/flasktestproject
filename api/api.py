import time
from flask import Flask, render_template, jsonify, request
import logging
import os.path as path
import datetime
import os
from flask_cors import CORS
import json

app = Flask(__name__, static_folder='../build', static_url_path='/')
app.config['CORS_HEADERS'] = 'Content-Type' # cross-site script 관련 문제 해결 

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(filename)s, %(module)s, %(funcName)s, [%(levelname)s] {%(lineno)d} : %(message)s",
    datefmt='%m-%d %H:%M',
    handlers=[
        logging.FileHandler(path.join( os.getcwd(),'Server_{:%Y%m%d}.log'.format(datetime.date.today()) ) ),
        logging.StreamHandler()
    ]
)

@app.route('/api/sendmsg', methods = ['POST'])
def get_query_from_react():
    logging.debug("get_query_from_react start")
    data = request.get_json()
    
    if data is None : 
        msg='None'
        logging.debug("None")
    else : 
        logging.debug(data)
        msg=data['text']
    return f"{msg}, Server Returned"
    
CORS(app)
