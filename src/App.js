import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import {render} from 'react-dom';

function postMsg(msg){
  alert(msg);

  var data={
    text:msg
  }

  axios( {
      method: 'POST',
      headers: { 'content-type': 'application/json; charset=UTF-8' },
      data: JSON.stringify(data),
      url:'http://localhost:5000/api/sendmsg'
    })
    .then(function(res){
        var parsed = JSON.parse(JSON.stringify(res));
        if (res && parsed['data']){
          alert(parsed['data']);
        }
    })
    .catch(function(error){
      console.log(error);
    });
}

function App() {  
  return (<script>
    {postMsg("React Client Send")}
  </script>);
}

export default App;
