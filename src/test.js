import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import React, { Component } from 'react';
import {render} from 'react-dom';

function App() {  
  function handleSubmit(e){
    e.preventDefault();
    var data={
      text: 'Client Send' 
    };

    fetch('http://localhost:5000/api/query',{
      method:'POST',
      headers:{
        'Content-Type':'application/json',
      },
      body: JSON.stringify(data),
    })
    .then(res => res.json())
    .then(res => console.log(res))
  }

  return (
    <section id="app">
      <form action="" onSubmit={handleSubmit}>
      <button> submit </button>
      </form>
    </section>
  );
  
}

export default App;

render(<App />, document.querySelector('#root'));
